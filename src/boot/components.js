import Vue from 'vue'
import VueMobileDetection from "vue-mobile-detection"
import VTooltip from 'v-tooltip'

Vue.use(VueMobileDetection)
Vue.use(VTooltip)
