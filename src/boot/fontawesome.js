import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleRight, faBars, faAngleDoubleRight, faChevronLeft, faPen, faSignOutAlt, faUser, faUsers } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faAngleRight, faBars, faAngleDoubleRight, faChevronLeft, faPen, faSignOutAlt, faUser, faUsers)

Vue.component('font-awesome-icon', FontAwesomeIcon)
