import Vue from 'vue'
import Vuex from 'vuex'

import user from '@/store/user'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',

  modules: {
    user
  },

  state: {
    isBusy: false
  },

  mutations: {
    setIsBusy(state, isBusy) {
      state.isBusy = isBusy
    }
  }
})
