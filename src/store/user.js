import userResource from '@/services/user'

export default {
  namespaced: true,
  state: () => ({
    users: [],
    user: {}
  }),

  mutations: {
    SET_USER(state, user) {
      state.user = user
    },

    SET_USER_POSTS(state, posts) {
      state.user.posts = posts
    },

    SET_USERS(state, users) {
      state.users = users
    }
  },

  actions: {
    async editUser({ commit }, user) {
      try {
        commit('setIsBusy', true, { root: true })
        await userResource.updateUser(user)
        commit('account/setAccount', user, { root: true })
      } finally {
        commit('setIsBusy', false, { root: true })
      }
    },
    
    async fetchUser({ commit }, id) {
      try {
        commit('setIsBusy', true, { root: true })
        const user = await userResource.getUser(id)
        const posts = await userResource.getUserPosts(id)
        commit('SET_USER', user)
        commit('SET_USER_POSTS', posts)
      } finally {
        commit('setIsBusy', false, { root: true })
      }
    },

    async fetchUsers({ commit }) {
      try {
        commit('setIsBusy', true, { root: true })
        const users = await userResource.getUsers()
        commit('SET_USERS', users)
      } finally {
        commit('setIsBusy', false, { root: true })
      }
    }
  }
}
