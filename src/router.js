import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/usuarios',
      name: 'userIndex',
      component: () => import(/* webpackChunkName: "userIndex" */ './views/user/Index.vue')
    },
    {
      path: '/usuarios/:id/editar',
      name: 'userEdit',
      props: true,
      component: () => import(/* webpackChunkName: "userEdit" */ './views/user/Edit.vue')
    }
  ]
})
