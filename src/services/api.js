import axios from 'axios'
import store from '@/store'

export default {
  init() {
    this.setBaseURL(process.env.VUE_APP_API_URL)
    this.interceptors()
  },

  setBaseURL(url) {
    axios.defaults.baseURL = url
  },

  get(resource, data, config) {
    return axios.get(resource, data, config)
  },

  post(resource, data, config) {
    return axios.post(resource, data, config)
  },

  put(resource, data, config) {
    return axios.put(resource, data, config)
  },

  delete(resource, config) {
    return axios.delete(resource, config)
  },

  /**
   * Perform a custom Axios request.
   *
   * data is an object containing the following properties:
   *  - method
   *  - url
   *  - data ... request payload
   *  - auth (optional)
   *    - username
   *    - password
  **/
  customRequest(data) {
    return axios(data)
  },

  interceptors() {
    axios.interceptors.response.use(this.manageSuccess, this.manageErrors)
  },

  manageSuccess(r) {
    return r
  },

  manageErrors(e) {
    if (!e.response || e.response.config.silentError) {
      return Promise.reject(e)
    }

    const status = e.response.status

    if (status === 401 && store.getters.isLoggedIn) {
      store.dispatch('auth/logout')
      this.$router.push({ name: 'login' })
    } else if (status === 404) {
      console.log({ type: 'error', message: 'No encontrado' })
    } else if (status === 403) {
      console.log({ type: 'error', message: 'Acceso denegado' })
    } else if (status === 400) {
      console.log({ type: 'error', message: e.response.data.title })
    } else {
      console.log({ type: 'error', message: 'Error interno o de conexión' })
    }

    return Promise.reject(e)
  }
}
