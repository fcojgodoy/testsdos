import Api from './api'

export default {
  async getUser(id) {
    return Api.get(`users/${id}`).then(r => r.data)
  },

  async getUsers() {
    return Api.get('users').then(r => r.data)
  },

  async getUserPosts(id) {
    return Api.get(`posts?userId=${id}`).then(r => r.data)
  }
}
