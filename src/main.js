import './boot'
import '@/assets/styles/main.scss'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Services from '@/services/api'

Vue.config.productionTip = false

// Load Montserrat typeface
require('typeface-montserrat')

Services.init()

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
